`timescale 1ns / 1ps
// SPDX-FileCopyrightText: 2020 Efabless Corporation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// SPDX-License-Identifier: Apache-2.0

`default_nettype none
/*
 *-------------------------------------------------------------
 *
 * user_project_wrapper
 *
 * This wrapper enumerates all of the pins available to the
 * user for the user project.
 *
 * An example user project is provided in this wrapper.  The
 * example should be removed and replaced with the actual
 * user project.
 *
 *-------------------------------------------------------------
 */

module FSM #(
    parameter BITS = 16
) (
`ifdef USE_POWER_PINS
    inout vccd1,	// User area 1 1.8V supply
    inout vssd1,	// User area 1 digital ground
`endif




    // Logic Analyzer Signals
    input  [127:0] la_data_in,
    output [127:0] la_data_out,
    
        // Independent clock (on independent integer divider)
    input   user_clock2

);
/*--------------------------------------*/
/* User project is instantiated  here   */
/*--------------------------------------*/

wire [15:0] data_in, data_out;
wire clk, reset, series_ready;

assign clk=user_clock2;
assign reset=la_data_in[33];
assign data_in=la_data_in[32:17];
assign data_out=la_data_out[32:17];
assign series_ready=la_data_out[33];

TOP
    #(16)//Module parametrization             //
    project(                                                         //
        // Inputs                                             //
        clk,                           //
        reset,                           //
        data_in[15:0],
        // Output
        data_out[15:0],                           //
        series_ready
    );


endmodule	// user_project_wrapper




module TOP
    #(parameter BIT = 16)//Module parametrization             //
    (                                                         //
        // Inputs                                             //
        input   clk,                            //
        input   reset,                           //
        input   [ BIT - 1 : 0 ] external_bus,
        // Output
        output  [ BIT - 1 : 0 ] output_bus,                           //
        output  series_ready
    );
    
wire [BIT-1:0] x_tent,x_log,x,CL,CT,internal_bus,x_n;
wire  [2:0] en_mode;
wire ldX,ldL,ldT,ldE;
assign output_bus = x;
controller
 #(BIT)//Module parametrization             //
 control_block(
    clk,
    reset,
    external_bus,
    x_n,
    ldE,
    ldX,
    ldL,
    ldT,
    internal_bus,
    series_ready 

    );
ld_parameter
    #(BIT)//Module parametrization             //
    X(        
        clk,
        ldX,
        internal_bus,        
        x
     )  ;
ld_parameter
    #(BIT)//Module parametrization             //
    C_L(        
        clk,
        ldL,
        internal_bus,        
        CL
     )  ;
ld_parameter
    #(BIT)//Module parametrization             //
    C_T(        
        clk,
        ldT,
        internal_bus,        
        CT
     )  ;
ld_parameter
    #(3)//Module parametrization             //
    mode(        
        clk,
        ldE,
        internal_bus[2:0],        
        en_mode
     )  ;
map
    #(BIT)//Module parametrization             //
    a(        
        x,
        CL,
        CT,
        
        x_tent,
        x_log
     )  ;
                                               //
decoder
    #(BIT)//Module parametrization             //
    b(                                                         //
        // Inputs                                             //
      x_tent,                       //
      x_log,                        //
    en_mode,        
        // Output
        x_n                           //
    );
endmodule
module decoder
    #(parameter BIT = 16)//Module parametrization             //
    (                                                         //
        // Inputs                                             //
        input   [ BIT - 1 : 0 ] x_tent,                       //
        input   [ BIT - 1 : 0 ] x_log,                        //
        input   [  2      : 0 ] en_mode,        
        // Output
        output  [ BIT - 1 : 0 ] x_n                           //
    );
wire [BIT -1 :0] flipped,unflipped,NLCS,FPCS,to_flip;
wire [ 2*BIT - 4 : 0 ] FPCS_temp; 
wire [BIT -1 :0] double_map,single_map;



assign to_flip   = (~en_mode[0]) ? (x_tent):(x_log);
assign unflipped = (~en_mode[1]) ? (x_tent):(x_log);

assign flipped= {~|to_flip[BIT-1:0],~to_flip[BIT-2:0]+1'b1};
assign FPCS_temp= flipped*unflipped;
assign NLCS= x_tent+x_log;
assign FPCS =FPCS_temp[ 2*BIT - 4 : BIT   - 3 ];

assign double_map = (~en_mode[1] | en_mode[0]) ? (FPCS):(NLCS);
assign single_map = (en_mode[1] ^  en_mode[0]) ? (flipped):(unflipped);
assign x_n        = (~en_mode[2]             ) ? (double_map):(single_map);



endmodule
module ld_parameter
    #(parameter BIT = 16)//Module parametrization             //
    (    
        input clk,
        input ld,
        input [BIT-1:0] internal_bus,
        output reg [BIT -1:0] para_meter  
    );

always @(posedge clk)
    begin
        if (ld) para_meter <= internal_bus;
    end
endmodule
module map
    #(parameter BIT = 16)//Module parametrization             //
    (                                                         //
        // Inputs                                             //
        input   [ BIT - 1 : 0 ] x,                            //
        input   [ BIT - 1 : 0 ] CL,                           //
        input   [ BIT - 1 : 0 ] CT, 
                                                              //
        // Outputs                                            //
        output  [ BIT - 1 : 0 ] x_tent,                        //
        output  [ BIT - 1 : 0 ] x_log                         //
    );
wire [BIT -1 :0] one_minux_x, x_input;
wire [ 2*BIT -3 : 0 ] tent;                          //
wire [ 3*BIT -5 : 0 ] log; 
wire router;


assign router = x[BIT-2]||x[BIT-1];

assign one_minux_x= {~|x[BIT-1:0],~x[BIT-2:0]+1'b1};

assign x_input = (router) ? (one_minux_x):(x);


assign tent = x_input* CT;
assign log  = x*one_minux_x * CL;

assign x_tent = tent[ 2*BIT - 3 : BIT   - 2 ];
assign x_log  = log [ 3*BIT - 5 : 2*BIT - 4 ];

endmodule





module controller
 #(parameter BIT = 16)//Module parametrization             //
 (
    input CLK,
    input reset,
    input [BIT-1:0]external_bus,
    input [BIT-1:0] x_n,
    output reg ldE,
    output reg ldX,
    output reg ldL,
    output reg ldT,
    output  [BIT-1:0] internal_bus,
    output series_ready 

    );
reg [2:0] state;
localparam S0=3'b000,S1=3'b001,S2=3'b010,S3=3'b011,S4=3'b100,S5=3'b101,S6=3'b110,S7=3'b111; 
reg [12:0] counter=0;
reg bus_mode=1;


wire control_signal,repeate_signal;
assign control_signal=&external_bus[BIT-1:BIT-3];
assign repeate_signal=(&counter);
assign internal_bus=(bus_mode)?(external_bus): (x_n);
assign series_ready = ldX;

always @(posedge CLK )
    begin
        case (state)
            S0: if (control_signal) state <=S1;
            S1: state<=S2;
            S2: state<=S3;
            S3: state<=S4;
            S4: begin state<=S5; counter<=0; end
            S5:begin 
                counter<=counter+1;                
                if (reset) state <= S7;
                else if (control_signal) state <= S6;
               end
            S6: if (control_signal) state <= S5;
            S7: state<=S0;
            default :state <=S0;
        endcase
     end
always @(state or counter)
    begin
        case (state)
            S0: begin ldE = 0; ldX = 0; ldL = 0; ldT = 0; bus_mode =1 ;end
            S1: begin ldE = 1; ldX = 0; ldL = 0; ldT = 0; bus_mode =1 ;end
            S2: begin ldE = 0; ldX = 1; ldL = 0; ldT = 0; bus_mode =1 ;end
            S3: begin ldE = 0; ldX = 0; ldL = 1; ldT = 0; bus_mode =1 ;end
            S4: begin ldE = 0; ldX = 0; ldL = 0; ldT = 1; bus_mode =1 ;end
            S5: begin 
                if (counter==13'b1111111111111) begin
                    ldX=1;
                end else begin ldX=0; end
                bus_mode = 0 ;ldE = 0;  ldL = 0; ldT = 0;
            end
            S6: begin ldE = 0; ldX = 0; ldL = 0; ldT = 0;  bus_mode = 0 ; end
            S7: begin ldE = 1; ldX = 1; ldL = 1; ldT = 1;  bus_mode = 1 ; end
        endcase
    end
              
endmodule
`default_nettype wire
